/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.interfaceanimal;

/**
 *
 * @author nonnanee
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("van");
        Bird bird = new Bird("sun");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Dan");
        Human human = new Human("min");
        Cat cat = new Cat("ben");
        Car car = new Car("Engine number I");
        
       Flyable[]flyables = {bat,bird,plane};
       for(Flyable f : flyables){
           if(f instanceof Plane){
           Plane p = (Plane)f;
           p.startEngine();
           p.run();
       }
           f.fly();
       }
       Runable[]runable = {human,cat,dog,plane,car};
       for(Runable r: runable){
           if(r instanceof Car){
           Car c= (Car)r;
           c.startEngine();
         
       }
           r.run();
    }
    }
}
